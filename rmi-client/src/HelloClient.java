import java.util.Scanner;
import java.util.Random;
import java.io.UnsupportedEncodingException;
import java.rmi.registry.*;

public class HelloClient {
	static Scanner scanner = new Scanner(System.in);

	static byte[] encrypt(String message, int key) throws UnsupportedEncodingException {
		Random generator = new Random(key);
		byte[] messageBytes = message.getBytes("utf-8");
		byte[] randomBytes = new byte[messageBytes.length];
		byte[] encryptedBytes = new byte[messageBytes.length];
		generator.nextBytes(randomBytes);

		for (int i = 0; i < messageBytes.length; i++) {
			encryptedBytes[i] = (byte)(messageBytes[i] ^ randomBytes[i]);
		}
		return encryptedBytes;
	}


	static String decrypt(byte[] encryptedBytes, int key) throws UnsupportedEncodingException {
		Random generator = new Random(key);
		byte[] randomBytes = new byte[encryptedBytes.length];
		byte[] messageBytes = new byte[encryptedBytes.length];
		generator.nextBytes(randomBytes);

		for (int i = 0; i < encryptedBytes.length; i++) {
			messageBytes[i] = (byte)(encryptedBytes[i] ^ randomBytes[i]);
		}
		return new String(messageBytes, "utf-8");
	}

	public static void main(String args[]) {
		try {
			String ip = "0.0.0.0";
			int port = 10900;

			Registry reg = LocateRegistry.getRegistry(ip, port);
			Hello stub = (Hello)reg.lookup("Hello");

			int key = Integer.parseInt(args[1]);
			while (true) {
				System.out.print("You > ");
				String clientMessage = scanner.nextLine();
				byte[] response = stub.message(encrypt(clientMessage, key));
				System.out.println("S   > " + decrypt(response, key));
			}
		}

		catch (Exception e) {
			System.out.println("Err: " + e.toString());
		}

	}

}
