import java.rmi.*;

public interface Hello extends Remote {
	byte[] message(byte[] message) throws RemoteException;
}
