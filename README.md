# Java RMI

Symmetric encryption is used to encrypt the communication. The encryption key needs to be supplied as an command-line argument. Encryption key is an integer and it is used as a seed to a random generator.

## Compilation

```sh
cd rmi-client
javac -d bin/ src/*.java
```

```sh
cd rmi-server
javac -d bin/ src/*.java
```

## Running

```sh
cd rmi-client
java -classpath bin HelloClient -- <int-key>
```

```sh
cd rmi-server
java -classpath bin HelloServer -- <int-key>
```
