import java.rmi.registry.*;
import java.rmi.server.*;

public class HelloServer{

	public static void main(String args[]) {
		int port = 10900;
		String ip = "0.0.0.0";
		System.setProperty("java.rmi.server.hostname", ip);
		Registry reg = null;

		HelloImplementation hi = new HelloImplementation(Integer.parseInt(args[1]));
		try {
			Hello stub = (Hello)UnicastRemoteObject.exportObject(hi, 0);
			reg = LocateRegistry.createRegistry(port);
			reg.rebind("Hello", stub);
			System.out.println("Server has been started.");
		}
		catch (Exception e) {
			System.out.println("ERROR: " + e.toString());
		}
	}
}
