import java.util.Scanner;
import java.util.Random;
import java.io.UnsupportedEncodingException;
import java.rmi.registry.*;

public class HelloImplementation implements Hello {
	Scanner scanner = new Scanner(System.in);
	int key;

	HelloImplementation(int key) {
		this.key = key;
	}


	byte[] encrypt(String message, int key) throws UnsupportedEncodingException {
		Random generator = new Random(key);
		byte[] messageBytes = message.getBytes("utf-8");
		byte[] randomBytes = new byte[messageBytes.length];
		byte[] encryptedBytes = new byte[messageBytes.length];
		generator.nextBytes(randomBytes);

		for (int i = 0; i < messageBytes.length; i++) {
			encryptedBytes[i] = (byte)(messageBytes[i] ^ randomBytes[i]);
		}
		return encryptedBytes;
	}


	String decrypt(byte[] encryptedBytes, int key) throws UnsupportedEncodingException {
		Random generator = new Random(key);
		byte[] randomBytes = new byte[encryptedBytes.length];
		byte[] messageBytes = new byte[encryptedBytes.length];
		generator.nextBytes(randomBytes);

		for (int i = 0; i < encryptedBytes.length; i++) {
			messageBytes[i] = (byte)(encryptedBytes[i] ^ randomBytes[i]);
		}
		return new String(messageBytes, "utf-8");
	}


	public byte[] message(byte[] message) {
		try {
			System.out.println("C   > " + decrypt(message, this.key));
			System.out.print("You > ");
			String serverMessage = scanner.nextLine();
			return encrypt(serverMessage, this.key);
		} catch (UnsupportedEncodingException e) {
			System.out.println("ERROR: " + e.toString());
			return new byte[0];
		}
	}
}
